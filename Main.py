import sys
from PyQt5 import QtWidgets, QtGui
from py_translator import Translator

class Window(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        self.writing = QtWidgets.QLabel('Choose the language of translation:')
        self.writing.setStyleSheet("font-variant: small-caps; font-family: 'Comic Sans MS'; font-weight: bold; font-size: 16px")
        self.btn1 = QtWidgets.QRadioButton('Polish')
        self.btn1.setStyleSheet(" font-family: 'Comic Sans MS';"
                                "font-size: 15px;")
        self.btn2 = QtWidgets.QRadioButton('English')
        self.btn2.setStyleSheet(" font-family: 'Comic Sans MS';"
                                " font-size: 15px;")
        self.btn3 = QtWidgets.QRadioButton('Spanish')
        self.btn3.setStyleSheet(" font-family: 'Comic Sans MS';"
                                " font-size: 15px;")
        self.btn4 = QtWidgets.QRadioButton('French')
        self.btn4.setStyleSheet(" font-family: 'Comic Sans MS';"
                                " font-size: 15px;")
        self.btn5 = QtWidgets.QRadioButton('Italian')
        self.btn5.setStyleSheet(" font-family: 'Comic Sans MS';"
                                " font-size: 15px;")
        self.btn6 = QtWidgets.QRadioButton('German')
        self.btn6.setStyleSheet(" font-family: 'Comic Sans MS';"
                                " font-size: 15px;")
        self.pbutton1 = QtWidgets.QPushButton('Translate')
        self.pbutton1.setStyleSheet("border-radius: 4px;"
                                    " background-color: #3D81B6;"
                                    " padding: 10px;"
                                    " font-variant: small-caps;"
                                    " font-family: 'Comic Sans MS';"
                                    " color: white;"
                                    " font-weight: bold;"
                                    " font-size: 15px;")
        self.pbutton2 = QtWidgets.QPushButton('Clear')
        self.pbutton2.setStyleSheet("border-radius: 4px;"
                                    " background-color: #3D81B6;"
                                    " padding: 10px;"
                                    " font-variant: small-caps;"
                                    " font-family: 'Comic Sans MS';"
                                    " color: white;"
                                    " font-weight: bold;"
                                    " font-size: 15px;")
        self.label = QtWidgets.QTextEdit()
        self.label.setStyleSheet("border: 3px solid dodgerblue; "
                                 "border-radius: 4px; "
                                 "background-color: white;"
                                 "font-family: 'Comic Sans MS';"
                                 "font-weight: bold;"
                                 "font-size: 17px;")
        self.label2 = QtWidgets.QTextEdit()
        self.label2.setText('Your translation will appear here!')
        self.label2.setStyleSheet("border: 3px solid dodgerblue;"
                                  "border-radius: 4px;"
                                  "background-color: white;"
                                  "font-family: 'Comic Sans MS';"
                                  "font-weight: bold;"
                                  "font-size: 17px;")
        self.label2.setReadOnly(True)

        h_box = QtWidgets.QHBoxLayout()
        h_box.addWidget(self.pbutton1)
        h_box.addWidget(self.pbutton2)
        h_box.addWidget(self.label)
        h_box.addWidget(self.label2)

        v_box = QtWidgets.QVBoxLayout()
        v_box.addStretch()
        v_box.addWidget(self.writing)
        v_box.addWidget(self.btn1)
        v_box.addWidget(self.btn2)
        v_box.addWidget(self.btn3)
        v_box.addWidget(self.btn4)
        v_box.addWidget(self.btn5)
        v_box.addWidget(self.btn6)
        v_box.addStretch()
        v_box.addLayout(h_box)

        self.btn1.clicked.connect(self.btn_clicked)
        self.btn2.clicked.connect(self.btn_clicked)
        self.btn3.clicked.connect(self.btn_clicked)
        self.btn4.clicked.connect(self.btn_clicked)
        self.btn5.clicked.connect(self.btn_clicked)
        self.btn6.clicked.connect(self.btn_clicked)

        self.pbutton1.clicked.connect(self.btn_translate_clr)
        self.pbutton2.clicked.connect(self.btn_translate_clr)

        self.setLayout(v_box)
        self.setWindowTitle('Desktop Translation Project')
        self.setWindowIcon(QtGui.QIcon('Icon.jpg'))
        self.show()

        self.a = ''

    def btn_clicked(self):
        sender = self.sender()
        if sender.text() == 'Polish':
            self.a = 'pl'
        elif sender.text() == 'English':
            self.a = 'en'
        elif sender.text() == 'Spanish':
            self.a = 'es'
        elif sender.text() == 'French':
            self.a = 'fr'
        elif sender.text() == 'Italian':
            self.a = 'it'
        elif sender.text() == 'German':
            self.a = 'de'

    def btn_translate_clr(self):
        sender2 = self.sender()
        if sender2.text() == 'Translate':
            b = self.label.toPlainText()
            translation = Translator().translate(text=b, dest=self.a).text
            self.label2.setText(translation)
        else:
            self.label.clear()


app = QtWidgets.QApplication(sys.argv)
a_window = Window()
a_window.setGeometry(700, 400, 750, 250)
a_window.setStyleSheet("background-color: #DEFEC1")
sys.exit(app.exec_())


